package presentation;


import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

import java.net.URL;
import java.util.ResourceBundle;

public class TextController1 implements Initializable {

    @FXML
    private Button b1;

    @FXML
    private Button b2;

    @FXML
    private Label l1;

    @FXML
    private RadioButton rb1;

    @FXML
    private RadioButton rb2;

    @FXML
    private TextField t1;

    @FXML
    private TextField t2;

    @FXML
    private ToggleGroup tg1;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        b1.setOnAction(actionEvent -> {
            if (rb1.isSelected()){
                l1.setText(t1.getText()+t2.getText());
            }else {
                l1.setText(t2.getText()+t1.getText());
            }

        });
        b2.setOnAction(actionEvent -> {
            t1.setText("");
            t2.setText("");
            l1.setText("");
        });
    }
}

