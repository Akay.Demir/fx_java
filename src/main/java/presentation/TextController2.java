package presentation;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class TextController2 implements Initializable {

    @FXML
    private Button b1;

    @FXML
    private Button b2;

    @FXML
    private Label l1;

    @FXML
    private TextField t1;

    @FXML
    private TextField t2;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        b1.setOnAction(actionEvent -> {
            if (t1.getText().isBlank() || t2.getText().isBlank()){
                l1.setText("Both TextFields must be filled.");
            }else {
                String text1 = t1.getText();
                String text2 = t2.getText();

                t1.setText(text2);
                t2.setText(text1);

                l1.setText("Swapped");
            }

        });

        b2.setOnAction(actionEvent -> {
            t1.setText("");
            t2.setText("");
            l1.setText("");
        });
    }
}