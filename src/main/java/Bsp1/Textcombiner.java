package Bsp1;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.Objects;

public class Textcombiner extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/layout/bsp1.fxml")));
        GridPane gridPane= new GridPane();
        RadioButton radioButton1 = new RadioButton();
        RadioButton radioButton2 = new RadioButton();
        radioButton1.setId("rb1");
        radioButton2.setId("rb2");
        TextField textField1 = new TextField();
        TextField textField2 = new TextField();
        textField1.setId("t1");
        textField2.setId("t2");
        Label label1= new Label();
        label1.setId("l1");
        Button button1 = new Button();
        Button button2 = new Button();
        button1.setId("b1");
        button2.setId("b2");


        gridPane.getChildren().add(radioButton1);
        gridPane.getChildren().add(radioButton2);
        gridPane.getChildren().add(textField1);
        gridPane.getChildren().add(textField2);
        gridPane.getChildren().add(label1);
        gridPane.getChildren().add(button1);
        gridPane.getChildren().add(button2);



        stage.setScene(new Scene(root));
        stage.show();

    }
}
